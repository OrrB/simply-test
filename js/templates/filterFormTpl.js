module.exports = function(){
    return `
        <div class="filter-form">
            <div class="filter-form-title">Фильтр точек</div>
            <div class="filter-form-field">
                <label class="filter-form-field-label">Название места</label>
                <input class="filter-form-field-value" ng-model="$ctrl.filter.name" ng-change="$ctrl.filterHandler()" />
            </div>
            <div class="filter-form-field">
                <label class="filter-form-field-label">Описание места</label>
                <input class="filter-form-field-value" ng-model="$ctrl.filter.description" ng-change="$ctrl.filterHandler()" />
            </div>
            <div class="filter-form-field">
                <label class="filter-form-field-label">Тип</label>
                <div class="filter-form-field-type-list"><div class="filter-form-field-type" ng-repeat="(key, value) in $ctrl.placeTypes">
                        <input id="filter-type-{{key}}" class="filter-form-field-value" type="checkbox" name="filterType" value="{{key}}" ng-model="$ctrl.filter.categories[key]" ng-change="$ctrl.filterHandler()" />
                        <label for="filter-type-{{key}}">{{value}}</label>
                </div></div>
            </div>
            <div class="filter-form-field">
                <label class="filter-form-field-label">Наличие фото</label>
                <input class="filter-form-field-value" type="checkbox" ng-model="$ctrl.filter.hasPhoto" ng-change="$ctrl.filterHandler()" />
            </div>
        </div>
    `;
};
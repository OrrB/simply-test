module.exports = function(){
    return `
    <input class="add-point-btn" ng-click="$ctrl.addNewPoint()" ng-hide="$ctrl.showAddOverlay" type="button" value="Добавить точку" />
    <div class="add-point-form" ng-show="$ctrl.showAddOverlay">
        <div class="add-point-form-field">
            <label class="add-point-form-field-label" for="newPointName">Название места</label>
            <input id="newPointName" ng-model="$ctrl.newPoint.name">
        </div>
        <div class="add-point-form-field">
            <label class="add-point-form-field-label" for="newPointDesc">Описание места</label>
            <textarea id="newPointDesc" ng-model="$ctrl.newPoint.description"></textarea>
        </div>
        <div class="add-point-form-field">
            <label class="add-point-form-field-label">Тип точки</label>
            <div class="add-point-form-field-type-list">
                <div class="add-point-form-field-type" ng-repeat="(key, value) in $ctrl.placeTypes">
                    <input id="place-type-{{key}}" type="radio" name="placeType" ng-model="$ctrl.newPoint.type" value="{{key}}">
                    <label for="place-type-{{key}}">{{value}}</label>
                </div>
            </div>
        </div>
        <div class="add-point-form-field">
            <label class="add-point-form-field-label" for="newPointImg">Ссылка на фото</label>
            <input id="newPointImg" ng-model="$ctrl.newPoint.photo">
        </div>
        <input class="add-point-btn" ng-click="$ctrl.submitNewPoint()" type="button" value="Подтвердить" />
        <input class="add-point-btn" ng-click="$ctrl.cancelAddingPoint()" type="button" value="Отмена" />
    </div>
    `
};
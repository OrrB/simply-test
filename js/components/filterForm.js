const filterFormTpl = require('../templates/filterFormTpl');
const pointFilter = require('../models/pointFilter');
const placeTypes = require('../dicts/placeTypes');
const appPointList = require('../models/appPointList');

class FilterFormController{
    constructor(){
        this.points = appPointList.points;
        this.showFilterOverlay = false;
        this.placeTypes = placeTypes;
        this.filter = {
            name: '',
            description: '',
            categories: {},
            hasPhoto: false
        };
    }

    filterHandler(){
        let pointsFiltered = pointFilter(this.points, this.filter);
        for (let i = 0; i < this.points.length; i++){
            if (pointsFiltered.indexOf(this.points[i]) !== -1){
                this.points[i].showPoint();
            } else{
                this.points[i].hidePoint();
            }
        }
    }
}
angular.module('maps').component('filterForm', {
    template: filterFormTpl,
    controller: FilterFormController,
    bindings: {}
});
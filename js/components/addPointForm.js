const angular = require('angular');
const addPointFormTpl = require('../templates/addPointFormTpl');
const placeTypes = require('../dicts/placeTypes');
const map = require('../models/Map');
const Point = require('../models/Point');
const appPointList = require('../models/appPointList');
const DB = require('../models/DB');

class AddPointFormController{
    constructor(){
        this.showAddOverlay = false;
        this.placeTypes = placeTypes;
        this.newPoint = new Point();
        this.points = appPointList.points;
    }
    addNewPoint(){
        let mapCenter = map.getMap().getCenter();
        this.showAddOverlay = true;
        this.newPoint.addBasePointToMap(mapCenter.lat(), mapCenter.lng());
    }
    submitNewPoint(){
        let newPoint = this.newPoint;
        let pointPosition = newPoint.mapPointLink.getPosition();
        if ((newPoint.name === '') || (newPoint.type === '')){
            return false;
        }
        this.points.push(newPoint);
        newPoint.lat = pointPosition.lat();
        newPoint.lng = pointPosition.lng();
        newPoint.addPointToMap();
        DB.savePoints(this.points);
        this.newPoint = new Point();
        this.showAddOverlay = false;
    };
    cancelAddingPoint(){
        this.showAddOverlay = false;
        this.newPoint.mapPointLink.setMap(null);
        this.newPoint.mapPointLink = null;
    };
}
angular.module('maps').component('addPointForm', {
    template: addPointFormTpl,
    controller: AddPointFormController,
    bindings: {}
});
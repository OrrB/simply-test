const map = require('./Map');
const clusters = require('./clusters');
const DB = require('./DB');
const balloonTpl = require('../templates/balloonTpl');
const appPointList = require('./appPointList');

class Point{
    constructor(data){
        if (data !== undefined){
            this.name = data.name || '';
            this.type = data.type || '';
            this.description = data.description || '';
            this.photo = data.photo || '';
            this.lat = data.lat || 0;
            this.lng = data.lng || 0;
        } else{
            this.name = '';
            this.type = '';
            this.description = '';
            this.photo = '';
            this.lat = 0;
            this.lng = 0;
        }
        this.isShown = false;
        this.mapPointLink = null;
    }
    addBasePointToMap(lat, lng){
        if (lat !== undefined){
            this.lat = lat;
        }
        if (lng !== undefined){
            this.lng = lng;
        }
        this.mapPointLink = new google.maps.Marker({
            map: map.getMap(),
            position: new google.maps.LatLng(this.lat, this.lng),
            icon: (this.type) ? '/images/icons/'+this.type+'.png' : null,
            draggable: true
        });
        this.isShown = true;
        return this;
    }
    addPointToMap(lat, lng){
        if (this.mapPointLink === null){
            this.addBasePointToMap(lat, lng);
        }
        if (this.icon !== ''){
            this.mapPointLink.setIcon('/images/icons/'+this.type+'.png');
        }
        let balloon = new google.maps.InfoWindow({
            content: balloonTpl(this)
        });
        let latCache,
            lngCache;
        this.mapPointLink.addListener('click', () => {
            balloon.open(map.getMap(), this.mapPointLink);
        });
        if (this.type){
            clusters.getClusters()[this.type].addMarker(this.mapPointLink);
        }
        google.maps.event.addListener(this.mapPointLink, "dragstart", (event) => {
            latCache = event.latLng.lat();
            lngCache = event.latLng.lng();
        });
        google.maps.event.addListener(this.mapPointLink, "dragend", (event) => {
            if (confirm('Подтвердите перенос точки')){
                this.lat = event.latLng.lat();
                this.lng = event.latLng.lng();
                DB.savePoints(appPointList.points);
            } else{
                this.mapPointLink.setPosition(new google.maps.LatLng(latCache, lngCache));
            }
        });
        return this;
    }
    hidePoint(){
        if (this.isShown === true){
            clusters.getClusters()[this.type].removeMarker(this.mapPointLink);
            this.mapPointLink.setMap(null);
            this.isShown = false;
        }
    }
    showPoint(){
        if (this.isShown === false){
            this.mapPointLink.setMap(map.getMap());
            clusters.getClusters()[this.type].addMarker(this.mapPointLink);
            this.isShown = true;
        }
    }
}
module.exports = Point;
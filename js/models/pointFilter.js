module.exports = function(pointsList, filterInfo){
    let points = pointsList;
    let pointsFiltered = [];
    let filter = false;
    let categoriesEmpty = true;
    for (let key in filterInfo.categories){
        if (filterInfo.categories[key] === true){
            categoriesEmpty = false;
        }
    }
    for (let i = 0; i < points.length; i++){
        filter = false;
        if ((filterInfo.name !== '') && (points[i].name.toLowerCase().indexOf(filterInfo.name.toLowerCase()) === -1)){
            filter = true;
        }
        if ((filterInfo.description !== '') && (points[i].description.toLowerCase().indexOf(filterInfo.description.toLowerCase()) === -1)){
            filter = true;
        }
        if (!categoriesEmpty && (!(points[i].type in filterInfo.categories) || (filterInfo.categories[points[i].type] === false))){
            filter = true;
        }
        if ((filterInfo.hasPhoto === true) && (points[i].photo === '')){
            filter = true;
        }
        if (filter === false){
            pointsFiltered.push(points[i]);
        }
    }
    return pointsFiltered;
};
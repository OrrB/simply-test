let map = null;
window.initMap = () => {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 58.60567032964396, lng: 49.6747092666626},
        zoom: 14,
        mapTypeControl: false
    });
};

class Map{
    constructor(){}
    getMap(){
        let checkMap = (resolve, reject) => {
            if (map) {
                resolve(map);
            } else {
                setTimeout(checkMap, 0);
            }
        };
        return new Promise(checkMap);
    }
}

module.exports = {
    getMap: function(){
        return map;
    }
};
const Cluster = require('node-js-marker-clusterer');
const placeTypes = require('../dicts/placeTypes');
const map = require('./Map');

let clusters = {};
let isCreated = false;

module.exports = {
    getClusters: function(){
        if (!isCreated){
            for (let type in placeTypes){
                clusters[type] = new Cluster(map.getMap(), [], {
                    styles: [{
                        url: '/images/icons/'+type+'.png',
                        height: 37,
                        width: 32
                    }]
                });
            }
            isCreated = true;
        }
        return clusters;
    }
};

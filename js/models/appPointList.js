appPointList = {
    points: [],
    addPoint: function(point){
        this.points.push(point);
    },
    removePoint: function(point){
        let index = this.points.indexOf(point);
        if (index > -1){
            this.points.splice(index, 1);
        }
    }
};
module.exports = appPointList;
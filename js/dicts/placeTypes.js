module.exports = {
    tourism: 'Туризм',
    cafe: 'Кафе, рестораны',
    shopping: 'Шоппинг',
    nightlife: 'Ночная жизнь',
    services: 'Услуги и сервис',
    finance: 'Бизнес и финансы',
    entertainment: 'Развлечения',
    beauty: 'Красота и здоровье',
    transport: 'Транспорт',
    nature: 'Живая природа',
    beach: 'Пляжи',
    children: 'Детям',
    government: 'Госучреждения'
};
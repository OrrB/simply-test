const DB = require('./models/DB');
const Point = require('./models/Point');
const angular = require('angular');
const appPointList = require('./models/appPointList');
require('angularfire');

let app = {
    init: function(){
        this.mapsModule = angular.module('maps', ['firebase']);
        require('./components/addPointForm');
        require('./components/filterForm');
        DB.getValue((data) => {
            let currentPoint;
            for (let i = 0; i < data.points.length; i++){
                currentPoint = new Point(data.points[i]);
                appPointList.addPoint(currentPoint);
                currentPoint.addPointToMap();
            }
        });

        this.points = [];
    }
};
app.init();